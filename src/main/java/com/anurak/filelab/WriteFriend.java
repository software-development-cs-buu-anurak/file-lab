/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.filelab;

/**
 *
 * @author anu01
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WriteFriend {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Satida",20,"0612345678");
            Friend friend2 = new Friend("Patnaree",20,"0898765432");
            Friend friend3 = new Friend("Albert",20,"0923456789");
            File file = new File("friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}